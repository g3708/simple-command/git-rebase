# **Menyamakan branch main (production) dengan staging, kasus sebagai berikut :**

1. main mengalami perubahan karena perbaikan bug (hotfix)
2. staging mengalami perubahan karena penambahan fitur dan sudah lolos qa \
[**branch main dan staging mengalami update**](https://gitlab.com/g3708/simple-command/git-rebase/-/blob/main/screenshot/kasus-1/main-update-staging-update.png)


## langkah-langkah menerapkan
1. Fetch dan pull dari staging dan main
2. buatlah branch backup dari main dan staging (optional)
3. gabungkan staging dari main (menggunakan rebase)
4. samakan level local staging dengan local main
5. samakan level origin staging dengan local main, local staging
6. samakan level origin main dengan local main, local staging, dan origin staging

## command git yang digunakan
------ langkah 1 -------
```
git checkout main
git fetch
git pull
git checkout staging
git fetch
git pull
```

------ langkah 2 -------
```
git checkout -b bak-staging staging
git checkout -b bak-main main
```

----- langkah 3 -------
```
git checkout staging
git rebase main
```

------ langkah 4 ----------
```
git checkout main
git merge staging
```
[**Hasil akan sebagai berikut**](https://gitlab.com/g3708/simple-command/git-rebase/-/blob/main/screenshot/kasus-1/hasil-setelah-langkah-4.png)


------ langkah 5 ----------
```
git checkout staging
git push --force-with-lease
```

------ langkah 6 ----------
```
git checkout main
git push origin main
```
